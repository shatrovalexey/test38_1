<?php
	/** класс для согласованного выполнения программ пакета
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/

	namespace Application ;

	/**
	* @subpackage Application\Controller контроллер
	*/
	class Controller extends Base {
		/**
		* Создание объекта
		* @param stdclass $creator ссылка на объект-создатель
		*/
		public function __construct( $creator = null ) {
			// вызов метода родителя
			parent::__construct( $creator ) ;

			/**
			* @var Application\Model\Gear $gear объект модели шины
			*/
			$this->gear = new Model\Gear( $this ) ;

			/**
			* @var Application\Model\ListLine $list_line объект модели списка наименований для распознавания
			*/
			$this->list_line = new Model\ListLine( $this ) ;

			/**
			* @var Application\Model\RegExp $regexp объект модели правил распознавания
			*/
			$this->regexp = new Model\RegExp( $this ) ;
		}

		/**
		* Получение значений HTTP-аргументов.
		* @param array $name имена аргументов HTTP-запроса
		* @return mixed
		* если передано только одно имя аргумента, то возвращается скаляр,
		* если передано много имён, то возвращает массив
		*/
		protected function __arg( ) {
			$request = &$this->creator->request ;
			$result = array( ) ;

			foreach ( func_get_args( ) as $name ) {
				if ( ! isset( $request[ $name ] ) ) {
					$result[] = null ;

					continue ;
				}

				$result[] = $request[ $name ] ;
			}

			if ( count( $result ) == 1 ) {
				return $result[ 0 ] ;
			}

			return $result ;
		}

		/**
		* Оформление ответа для вывода в формате JSON
		* @return array инструкции для дальнейшей обработки запроса
		* view - имя файла представления
		* result - дополнительные данные для обработки запроса
		* result.data - данные для вывода в теле HTTP-ответа
		* result.passthru - выводить представление не внутри общего представления, а сразу
		* result.key - имя переменной для присвоения массива сообщений
		*/
		protected function __json( &$data ) {
			return array(
				'result' => array(
					'data' => $data
				) ,
				'view' => 'json' ,
				'headers' => array(
					array(
						$this->creator->config->http->header->default_name ,
						$this->creator->config->http->header->javascript
					)
				) ,
				'key' => 'message' ,
				'passthru' => true
			) ;
		}

		/**
		* Главная страница
		* @return array инструкции для дальнейшей обработки запроса
		* view - имя файла представления
		* result - дополнительные данные для обработки запроса
		*/
		public function indexAction( ) {
			return array(
				'view' => 'index' ,
				'result' => array( )
			) ;
		}

		/**
		* Распознавание атрибутов наименований шин из строки
		* @return array - хэш-массив с атрибутами шины
		*/
		private function __gear_from_text_line( $line , &$regexp_list ) {
			$result = array( ) ;

			foreach ( $regexp_list as $key => $regexp ) {
				$line = preg_replace_callback( '{' . $regexp[ 'regexp' ] . '}' . $regexp[ 'modifiers' ] ,
					function( $matches ) use( &$result , &$regexp ) {
						foreach ( $regexp[ 'fields' ] as $i => $field ) {
							$result[ $field ] = @$matches[ $i + 1 ] ;
						}

						return '' ;
					} , $line , 1 , $found
				) ;

				// пропуск, если набор полей был обязательным, но не был обнаружен
				if ( empty( $found ) && ! empty( $regexp[ 'required' ] ) ) {
					return array( ) ;
				}

				// все ненайденные поля должны иметь значение null
				foreach ( $regexp[ 'fields' ] as $field ) {
					if ( isset( $result[ $field ] ) ) {
						continue ;
					}

					$result[ $field ] = null ;
				}
			}

			// особая обработка некоторых полей
			$result[ 'studded' ] = intval( empty( $result[ 'not_studded' ] ) ) ;
			$result[ 'runflat' ] = ( $result[ 'runflat1' ] ? 'RunFlat' : $result[ 'runflat2' ] ) ;

			// так как предполагается список аббревиатур, то будет удобнее их обрабатывать с помощью функции find_in_set
			$result[ 'chars' ] = preg_replace( '{\(\s*\)|[/\*]|\s+$}usx' , ',' , $result[ 'chars' ] ) ;
			$result[ 'chars' ] = trim( $result[ 'chars' ] ) ;

			if ( empty( $result[ 'chars' ] ) ) {
				$result[ 'chars' ] = null ;
			}

			return $result ;
		}

		/**
		* Распознавание атрибутов наименований шин из списка
		* @return array инструкции для дальнейшей обработки запроса
		*/
		public function gear_detectAction( ) {
			$regexp = $this->regexp->all( ) ;
			$this->list_line->truncate( ) ;
			$this->gear->truncate( ) ;

			foreach ( explode( "\n" , $this->__arg( 'list' ) ) as $line ) {
				if ( $row = $this->__gear_from_text_line( $line , $regexp ) ) {
					$gear_id = $this->gear->add( $row ) ;
				}

				$result_current[ 'line_id' ] = $this->list_line->add( array(
					'line' => $line ,
					'gear_id' => ( empty( $row ) ? null : $gear_id )
				) ) ;
			}

			$columns = array( ) ;

			foreach ( $this->gear->columns( ) as $column ) {
				if ( empty( $column[ 'comment' ] ) ) {
					continue ;
				}

				$columns[ $column[ 'name' ] ] = $column[ 'comment' ] ;
			}

			$result = array(
				'columns' => $columns ,
				'rows' => $this->list_line->all( )
			) ;

			return $this->__json( $result ) ;
		}
	}
