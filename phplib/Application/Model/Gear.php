<?php
	/** пакет моделей
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	namespace Application\Model ;

	/** класс модели шины
	* @subpackage \Application\Model модель
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	class Gear extends \Application\Model {
		/**
		* Очистка таблицы `gear`
		* @return void
		*/
		public function truncate( ) {
			$sth = $this->dbh->prepare( '
TRUNCATE `gear` ;
			' ) ;
			$sth->execute( ) ;
		}

		/**
		* Добавление записи в таблицу `gear`
		* @param array $row - хэш-массив атрибутов
		* @return int - идентификатор записи
		*/
		public function add( $row ) {
			$result = array( ) ;
			$sth = $this->dbh->prepare( '
INSERT INTO
	`gear`
SET
	`brand` := upper( ? ) ,
	`model` := upper( ? ) ,
	`width` := ? ,
	`height` := ? ,
	`construction` := upper( ? ) ,
	`diameter` := ? ,
	`load_index` := ? ,
	`speed_index` := ? ,
	`chars` := ? ,
	`runflat` := ? ,
	`chamberiness` := ? ,
	`season` := lower( ? ) ,
	`studded` := ? ;
			' ) ;
			$sth->execute( array(
				$row[ 'brand' ] , $row[ 'model' ] , $row[ 'width' ] , $row[ 'height' ] ,
				$row[ 'construction' ] , $row[ 'diameter' ] , $row[ 'load_index' ] , $row[ 'speed_index' ] ,
				$row[ 'chars' ] , $row[ 'runflat' ] , $row[ 'chamberiness' ] , $row[ 'season' ] ,
				$row[ 'studded' ]
			) ) ;
			return $this->dbh->lastInsertId( ) ;
		}
	}