<?php
	/** пакет моделей
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	namespace Application\Model ;

	/** класс модели списка наименований шин
	* @subpackage \Application\Model модель
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	class ListLine extends \Application\Model {
		/**
		* Очистка таблицы `list`
		* @return void
		*/
		public function truncate( ) {
			$sth = $this->dbh->prepare( '
TRUNCATE `list` ;
			' ) ;
			$sth->execute( ) ;
		}

		/**
		* Добавление записи в таблицу `list`
		* @param array( string , int = null ) - строка и идентификатор записи в `gear`
		* @return int $line_id - идентификатор записи в таблице `list`
		*/
		public function add( $row ) {
			$sth = $this->dbh->prepare( '
INSERT INTO
	`list`
SET
	`line` := ? ,
	`gear_id` := ? ;
			' ) ;
			$sth->execute( array(
				$row[ 'line' ] , $row[ 'gear_id' ]
			) ) ;
			return $this->dbh->lastInsertId( ) ;
		}

		/**
		* Выбор всех записей из таблицы `list`
		* @return array - список записей
		*/
		public function all( ) {
			$result = array( ) ;

			$sth = $this->dbh->prepare( '
SELECT SQL_BUFFER_RESULT SQL_BIG_RESULT
	`g1`.* ,
	`l1`.`line` ,
	( `l1`.`gear_id` IS null ) AS `error`
FROM
	`list` AS `l1`

	LEFT OUTER JOIN `gear` AS `g1` ON
	( `l1`.`gear_id` = `g1`.`gear_id` ) ;
			' ) ;
			$sth->execute( ) ;
			return $sth->fetchall( \PDO::FETCH_ASSOC ) ;
		}
	}