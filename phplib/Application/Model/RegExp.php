<?php
	/** пакет моделей
	* @package Application описанные классы задачи
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	namespace Application\Model ;

	/** класс модели списка рег.выражений для анализа наименования шины
	* @subpackage \Application\Model модель
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	class RegExp extends \Application\Model {
		/**
		* Выбор всех записей из таблицы `regexp`
		* @return array - хэш-массив "название" => "регулярное выражение"
		*/
		public function all( ) {
			$result = array( ) ;

			$sth = $this->dbh->prepare( '
SELECT
	`r1`.`name` ,
	`r1`.`value` ,
	`r1`.`modifiers` ,
	`r1`.`fields` ,
	`r1`.`required`
FROM
	`regexp` AS `r1`
ORDER BY
	`r1`.`order` ASC ;
			' ) ;
			$sth->execute( ) ;
			while ( list( $name , $value , $modifiers , $fields , $required ) = $sth->fetch( \PDO::FETCH_NUM ) ) {
				$result[ $name ] = array(
					'regexp' => $value ,
					'modifiers' => $modifiers ,
					'fields' => explode( ',' , $fields ) ,
					'required' => $required
				) ;
			}

			return $result ;
		}
	}