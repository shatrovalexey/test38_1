jQuery( function( ) {
	jQuery( "#gear_detect_form" ).on( "submit" , function( ) {
		let $form = jQuery( this ) ;
		let $output = $form.find( ".output" ) ;
		let $thead = $output.find( "thead" ) ;
		let $tbody = $output.find( "tbody" ) ;

		$output.addClass( "nod" ) ;
		$thead.empty( ) ;
		$tbody.empty( ) ;

		jQuery.ajax( {
			"url" : $form.attr( "action" ) ,
			"type" : $form.attr( "method" ) ,
			"data" : $form.serialize( ) ,
			"dataType" : "json" ,
			"success" : function( $result ) {
				let $th_was = { } ;

				for ( $i = 0 ; $i < $result.data.rows.length ; $i ++ ) {
					let $row = $result.data.rows[ $i ] ; 
					let $tr = jQuery( "<tr>" ) ;
					$tbody.append( $tr ) ;

					for ( let $field in $row ) {
						if ( $field == 'error' ) {
							$tr.addClass( "error" + $row[ $field ] ) ;

							continue ;
						}

						let $td = jQuery( "<td>" ).text( $row[ $field ] ).attr( "title" , $row[ $field ] ) ;
						$tr.append( $td ) ;

						if ( $field in $th_was ) {
							continue ;
						}

						let $label = $field ;

						if ( $field in $result.data.columns ) {
							$label = $result.data.columns[ $field ] ;
						}

						$th_was[ $field ] = true ;

						$thead.append( jQuery( "<th>" ).text( $label ).attr( "title" , $label ) ) ;
					}
				}

				$output.removeClass( "nod" ) ;

				return ;
			} ,
			"failure" : function( ) {
				alert( "внутрення ошибка" ) ;
			}
		} ) ;

		return false ;
	} ) ;
} ) ;