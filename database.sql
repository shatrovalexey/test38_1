CREATE DATABASE  IF NOT EXISTS `task38` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `task38`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: task38
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gear`
--

DROP TABLE IF EXISTS `gear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gear` (
  `gear_id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `brand` varchar(100) NOT NULL COMMENT 'брэнд',
  `model` varchar(100) DEFAULT NULL COMMENT 'модель',
  `width` decimal(3,0) NOT NULL COMMENT 'ширина',
  `height` decimal(3,0) NOT NULL COMMENT 'высота',
  `construction` char(1) CHARACTER SET latin1 NOT NULL COMMENT 'конструкция',
  `diameter` decimal(2,0) unsigned NOT NULL COMMENT 'диаметр',
  `load_index` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'индекс загрузки',
  `speed_index` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'индекс скорости',
  `chars` varchar(100) DEFAULT NULL COMMENT 'характеризующие аббревиатуры',
  `runflat` varchar(10) CHARACTER SET latin1 DEFAULT NULL COMMENT 'runflat',
  `chamberiness` varchar(10) DEFAULT NULL COMMENT 'камерность',
  `season` varchar(20) DEFAULT NULL COMMENT 'сезонность',
  `studded` tinyint(1) unsigned DEFAULT NULL COMMENT 'шипованность:\n* null = неизвестно;\n* 1 = "да";\n* 0 = "нет".',
  PRIMARY KEY (`gear_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='шины';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gear`
--

LOCK TABLES `gear` WRITE;
/*!40000 ALTER TABLE `gear` DISABLE KEYS */;
INSERT INTO `gear` VALUES (1,'TOYOH08','',195,75,'R',16,NULL,NULL,'C 107 105STL',NULL,NULL,'летние',1),(2,'PIRELLI','WINTER SNOWCONTROLSERIE 3',175,70,'R',14,'84','T',NULL,NULL,'TL','зимние',0),(3,'BFGOODRICH','MUD-TERRAIN T/A KM2',235,85,'R',16,'120','1','16Q  Внедорожные',NULL,'TL',NULL,1),(4,'PIRELLI','SCORPION ICE & SNOW',265,45,'R',21,'104','H',NULL,NULL,'TL','зимние',0),(5,'PIRELLI','WINTER SOTTOZEROSERIE II',245,45,'R',19,'102','V','XL','RunFlat','TL','зимние',0),(6,'PIRELLI','WINTER CARVING EDGE',225,50,'R',17,'98','T','XL',NULL,'TL','зимние',1),(7,'CONTINENTAL','CONTICROSSCONTACT LX SPORT',255,55,'R',18,'105','H','FR MO',NULL,'TL','всесезонные',1),(8,'BFGOODRICH','G-FORCE STUD',205,60,'R',16,'96','Q','XL',NULL,'TL','зимние',1),(9,'BFGOODRICH','WINTER SLALOM KSI',225,60,'R',17,'99','S',NULL,NULL,'TL','зимние',0),(10,'CONTINENTAL','CONTISPORTCONTACT 5',245,45,'R',18,'96','W','FR','SSR','TL','летние',1),(11,'CONTINENTAL','CONTIWINTERCONTACT TS 830 P',205,60,'R',16,'92','H',NULL,'SSR','TL','зимние',0),(12,'CONTINENTAL','CONTIWINTERCONTACT TS 830 P',225,45,'R',18,'95','V','XL  FR','SSR','TL','зимние',0),(13,'HANKOOK','WINTER I*CEPT EVO2 W320',255,35,'R',19,'96','V','XL  TT',NULL,'TL','зимние',0);
/*!40000 ALTER TABLE `gear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `list`
--

DROP TABLE IF EXISTS `list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list` (
  `list_id` bigint(22) unsigned NOT NULL AUTO_INCREMENT COMMENT 'идентификатор',
  `gear_id` bigint(22) unsigned DEFAULT NULL COMMENT 'идентификатор записи в `gear`',
  `line` longblob NOT NULL COMMENT 'строка',
  PRIMARY KEY (`list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COMMENT='входные данные';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `list`
--

LOCK TABLES `list` WRITE;
/*!40000 ALTER TABLE `list` DISABLE KEYS */;
INSERT INTO `list` VALUES (1,1,'ToyoH08 195/75R16C 107/105STL Летние\r'),(2,2,'Pirelli Winter SnowControlserie 3 175/70R14 84T TL Зимние (нешипованные)\r'),(3,3,'BFGoodrich Mud-Terrain T/A KM2 235/85R16 120/116Q TL Внедорожные\r'),(4,4,'Pirelli Scorpion Ice & Snow 265/45R21 104H TL Зимние (нешипованные)\r'),(5,5,'Pirelli Winter SottoZeroSerie II 245/45R19 102V XL Run Flat * TL Зимние (нешипованные)\r'),(6,NULL,'Nokian Hakkapeliitta R2 SUV/Е245/70R16 111R XL TL Зимние (нешипованные)\r'),(7,6,'Pirelli Winter Carving Edge 225/50R17 98T XL TL Зимние (шипованные)\r'),(8,7,'Continental ContiCrossContact LX Sport 255/55R18 105H FR MO TL Всесезонные\r'),(9,8,'BFGoodrich g-Force Stud 205/60R16 96Q XL TL Зимние (шипованные)\r'),(10,9,'BFGoodrich Winter Slalom KSI 225/60R17 99S TL Зимние (нешипованные)\r'),(11,10,'Continental ContiSportContact 5 245/45R18 96W SSR FR TL Летние\r'),(12,11,'Continental ContiWinterContact TS 830 P 205/60R16 92H SSR * TL Зимние (нешипованные)\r'),(13,12,'Continental ContiWinterContact TS 830 P 225/45R18 95V XL SSR FR * TL Зимние (нешипованные)\r'),(14,13,'Hankook Winter I*Cept Evo2 W320 255/35R19 96V XL TL/TTЗимние (нешипованные)\r'),(15,NULL,'Mitas Sport Force+ 120/65R17 56W TL Летние\r'),(16,NULL,'');
/*!40000 ALTER TABLE `list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regexp`
--

DROP TABLE IF EXISTS `regexp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regexp` (
  `name` varchar(20) CHARACTER SET latin1 NOT NULL COMMENT 'название',
  `value` longtext NOT NULL COMMENT 'регулярное выражение PCRE',
  `modifiers` varchar(10) DEFAULT 'usxi' COMMENT 'модификаторы рег.выражения',
  `fields` varchar(200) NOT NULL DEFAULT '' COMMENT 'список названий полей, соответствующих плэйсхолдерам выражения через '',''.',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'обязательное соответствие',
  `order` bigint(22) unsigned NOT NULL DEFAULT '0' COMMENT 'порядок выполнения',
  PRIMARY KEY (`name`),
  KEY `idx_order` (`order`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Регульрные выражения PCRE для поиска атрибутов шины в строке.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regexp`
--

LOCK TABLES `regexp` WRITE;
/*!40000 ALTER TABLE `regexp` DISABLE KEYS */;
INSERT INTO `regexp` VALUES ('chamberiness','\\b(TT|TL|TL/TT)\\b','usxi','chamberiness',0,4),('chars','(.+)','usx','chars',0,10),('main','(\\w+)	\\s*\n		(?:(.*?))?\\b	\\s+\n\n		(\\d+)\n			/\n		(\\d+)\n		([[:alpha:]])\n		(\\d{2})\n		(?:\n			\\s*\n			(\\d+)\\s*/?\\s*\n			(\\w\n				(?:\\([\\w+]\\))?\n			)\n		)?','usxi','brand,model,width,height,construction,diameter,load_index,speed_index',1,0),('runflat','\\b(?:\n			(\n				(?i)\n					run[\\*\\s]*flat\n				(?-i)\n			) | (\n				ROF	|\n				ZP	|\n				SSR	|\n				ZPS	|\n				HRS	|\n				RFT\n			)\n		)\\b','usx','runflat1,runflat2',0,3),('season','(\n	летние |\n	зимние |\n	всесезонн?ые\n)\\b','usxi','season',0,1),('studded','\\b(не\\s*)?(шипованн?ые)\\b','usxi','not_studded,studded',0,2);
/*!40000 ALTER TABLE `regexp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'task38'
--

--
-- Dumping routines for database 'task38'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-27 20:29:33
